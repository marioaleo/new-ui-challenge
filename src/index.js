//YOUR CODE HERE
const matchListNode = document.querySelector("#matchList");
const matchDetailNode = document.querySelector("#matchDetail");
const minAgeInput = document.querySelector("#minAge");
const maxAgeInput = document.querySelector("#maxAge");
const matchAmount = document.querySelector("#matchAmount");
const btnFilter = document.querySelector("#btnFilter");
const btnResetFilter = document.querySelector("#btnResetFilter");

let matchList = [];

const resetFilter = () => {
    minAgeInput.value = null;
    maxAgeInput.value = null;
    document.querySelectorAll("#filterGender input")[0].checked = true;
};

const calculateAge = date => Math.abs(new Date(new Date() - new Date(date).getTime()).getUTCFullYear() - 1970);
const validateGender = () => document.querySelector("#filterGender :checked").value !== "0" ?
    (document.querySelector("#filterGender :checked").value === "1" ? "&gender=male" :  "&gender=female") :
    "";

const fetchMatchList = () => fetch(`https://randomuser.me/api/?nat=US&results=10${ validateGender() }`, {
    method: "GET",
    headers: {
        "Accept": "application/json, application/xml, text/plain, text/html, *.*",
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
    }
});

const validateFetchResponse = response => {
    if (response.status !== 200)
        throw new Error(`Server Status ${ response.status }`);
    return response.json();
};

const populateMatchList = response => {
    matchList = response.results.filter(ele => {
        if (!minAgeInput.value)
            return true;
        else if (!maxAgeInput.value)
            return calculateAge(ele.dob) >= minAgeInput.value;
        else
            return calculateAge(ele.dob) >= minAgeInput.value && calculateAge(ele.dob) <= maxAgeInput.value;
    });
    matchAmount.innerHTML = `<span>${ matchList.length } Matches Found</span>`;
    matchListNode.innerHTML = matchList.reduce((acc, ele) => {
        return `
            ${ acc }
            <div class="matchListItem">
                <img src="${ ele.picture.thumbnail }">
                <div class="matchListItemInfo">
                    <span>
                        ${ ele.name.first } ${ ele.name.last } - ${ calculateAge(ele.dob) }
                    </span>
                    <span>
                        ${ ele.gender }
                    </span>
                </div>
            </div>
        `;
    }, "");
};

const requestMatchList = () => fetchMatchList()
    .then(validateFetchResponse)
    .then(populateMatchList)
    .catch(e => console.error(e));

btnFilter.onclick = requestMatchList;
btnResetFilter.onclick = resetFilter;
requestMatchList();
